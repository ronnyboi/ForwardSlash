using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]

public class Projectile : MonoBehaviour
{
    public ProjectileSO _projectile;
    private Vector3 _direction;
    private GameObject _player;

    private void Start()
    {
        _player = PlayerController.Instance.gameObject;
    }

    private void Update()
    {
        this.transform.position += _projectile.Direction * _projectile.Speed * Time.deltaTime;
        //Destroy(this.gameObject, _projectile.Lifetime);
        if (PlayerController.Instance.transform.position.x > 0)
        {
            _direction = transform.right;
        }
        else
        {
            _direction = transform.right;
        }
        _projectile.Direction = _direction;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Boundary"))
        {
            // Destroy(this.gameObject);
            this.gameObject.SetActive(false);
        }
    }
}
