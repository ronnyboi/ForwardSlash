using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance { get; private set; }

    [SerializeField] 
    private float _moveSpeed = 5f;
    [SerializeField] 
    private float _jumpForce = 500f;
    [SerializeField]
    private float _wallJumpForce = 500f;
    [SerializeField] 
    private int maxJumps = 2;
    private Vector3 velocity = Vector3.zero;
    private Vector3 _direction;


    [SerializeField]
    private Transform _groundCheck;
    [SerializeField]
    private Transform _wallCheck;
    [SerializeField]
    private LayerMask _groundLayer;
    [SerializeField]
    private LayerMask _wallLayer;

    //Projectile shooting
    public ProjectileManager projectileManager;
    public ProjectileSO _projectileSO;
    public UnityEvent<GameObject> shoot;
    [SerializeField]
    private GameObject _muzzle;
    private bool _canShoot = true;
    [SerializeField]
    private float _shootDelay = 0.5f;

    private Rigidbody2D _rigidBody;
    private SpriteRenderer _spriteRenderer;
    private bool _isJumping = false;
    private bool _isWallJumping = false;
    private bool _isAirborne = false;
    private bool _isGrounded = false;
    private bool _isTouchingWall;
    public bool _isFacingRight = true;


    private int jumpsRemaining;

    private float _movementInput;
    

    

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();

        if (Instance != null)
        {
            Debug.Log("There is more than one instance of PlayerController");
        }
        Instance = this;
    }

    private void Update()
    {

        // Check if the player is on the ground
        _isGrounded = Physics2D.OverlapCircle(_groundCheck.position, 0.2f, _groundLayer);

        // Check if the player is touching a wall
        _isTouchingWall = Physics2D.OverlapCircle(_wallCheck.position, 0.2f, _wallLayer);

        // Flip the player sprite based on movement direction
        _movementInput = Input.GetAxisRaw("Horizontal");
        

        // to flip the player left and right
        if (_movementInput > 0 && !_isFacingRight)
        {
            Flip();
        }
        else if (_movementInput < 0 && _isFacingRight)
        {
            Flip();
        }

        if (_isTouchingWall && Input.GetKeyDown(KeyCode.W))
        {
            _isWallJumping = true;
        }

        // Apply a jump force if the player is on the ground and presses the jump button
        if (_isGrounded && Input.GetKeyDown(KeyCode.W))
        {
            _isJumping = true;
        }
        else if (!_isGrounded && jumpsRemaining > 0 && Input.GetKeyDown(KeyCode.W))
        {
            _isAirborne = true;
        }
        if (Input.GetKeyDown(KeyCode.Mouse0) && _canShoot)
        {
            StartCoroutine(ShootDelay());
            Shoot();
        }

    }

    private void FixedUpdate()
    {
        MovePlayer();
        // Apply a wall jump force if the player is touching a wall and presses the jump button
        if (_isWallJumping)
        {
            WallJump();
        }

        // Apply a jump force if the player is on the ground and presses the jump button
        if (_isJumping)
        {
            Jump();
        }
        if (_isAirborne)
        {
            Jump();
            jumpsRemaining--;
        }
        
        _isWallJumping = false;
        _isJumping = false;
        _isAirborne = false;

    }

    private void MovePlayer()
    {
        // Move the player horizontally
        _movementInput = Input.GetAxisRaw("Horizontal") * _moveSpeed * Time.fixedDeltaTime;
        //_rigidBody.velocity = new Vector2(_movementInput, _rigidBody.velocity.y);

        // Move the character by finding the target velocity
        Vector3 targetVelocity = new Vector2(_movementInput * 10f, _rigidBody.velocity.y);

        // And then smoothing it out and applying it to the character
        _rigidBody.velocity = Vector3.SmoothDamp(_rigidBody.velocity, targetVelocity, ref velocity, .05f);


    }

    private void Jump()
    {
        _rigidBody.AddForce(new Vector2(0f, _jumpForce * Time.fixedDeltaTime), ForceMode2D.Impulse);
        //_rigidBody.AddForce(new Vector2(0f, _jumpForce));
        _isGrounded = false;
    }

    private void WallJump()
    {
        _rigidBody.velocity = new Vector2(_rigidBody.velocity.x, _jumpForce * Time.fixedDeltaTime);
        _rigidBody.AddForce(new Vector2((_isFacingRight ? -1f : 1f) * _wallJumpForce * Time.fixedDeltaTime, 0f), ForceMode2D.Impulse);
    }

    private void Flip()
    {
        _isFacingRight = !_isFacingRight;
        //_spriteRenderer.flipX = !_isFacingRight;
        Vector3 flipped = transform.localScale;
        flipped.z *= -1f;
        transform.localScale = flipped;

        transform.Rotate(0f, 180f, 0f);
    }

    private void Shoot()
    {
        // Shoot a laser
        //Instantiate(laserPrefab, muzzle.transform.position, Quaternion.identity);
        
        GameObject projectile = projectileManager.getProjectile();
        projectile.SetActive(true);
        projectile.transform.position = _muzzle.transform.position;
    }
    private IEnumerator ShootDelay()
    {
        _canShoot = false;
        Shoot();
        yield return new WaitForSeconds(_shootDelay);
        _canShoot = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Reset jumps remaining when colliding with the ground
        if (collision.gameObject.CompareTag("Ground"))
        {
            jumpsRemaining = maxJumps;
        }
        if (collision.gameObject.CompareTag("Wall"))
        {
            jumpsRemaining = maxJumps;
        }
    }
}