using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ProjectileScriptableObject", menuName = "ScriptableObjects/Projectile")]
public class ProjectileSO : ScriptableObject
{
    public Vector3 Direction;
    public float Speed;
    public float Lifetime;
}
