using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : MonoBehaviour
{
    [SerializeField]
    private int _sizeOfPool;
    [SerializeField]
    private GameObject _prefab;

    private List<GameObject> _poolGameObjects;

    void Start()
    {
        _poolGameObjects = new List<GameObject>();
        for (int i = 0; i < _sizeOfPool; i++)
        {
            GameObject projectile = Instantiate(_prefab);
            projectile.SetActive(false);
            _poolGameObjects.Add(projectile);
        }
    }

    public GameObject getProjectile()
    {

        for (int i = 0; i < _poolGameObjects.Count; i++)
        {
            if (!_poolGameObjects[i].activeInHierarchy)
            {
                return _poolGameObjects[i];
            }
        }

        GameObject projectile = Instantiate(_prefab);
        _poolGameObjects.Add(projectile);
        return projectile;
    }

    public void onShoot(GameObject spawner)
    {
        GameObject projectile = getProjectile();
        projectile.transform.position = spawner.transform.position;
        projectile.SetActive(true);
    }
}
